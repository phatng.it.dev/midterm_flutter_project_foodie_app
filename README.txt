Dạ thưa thầy về vấn đề không có lịch sử push code lên gitlab thì em xin trình bày thầy như sau ạ:
+ Trước khi chọn đề tài em đã tham khảo code và em tập code theo giống trên mạng nên lúc đó em không có push lên gitlab.
+ Lúc tới deadline thì em đã làm cho hoàn thành và push lại lên gitlab nên nó chỉ có 1 lần push.
+ Và giao diện bài của em thì em làm giống như bài Final Project môn UI/UX của em học kì này luôn ạ. Để cho nó đồng bộ cái mình thiết kế với mình code á thầy.
+ Do khả năng còn yếu kém nên em chỉ làm giống được bài UI/UX ở phần "Login" với phần "Profile" thôi ạ. Xin thầy thông cảm.

Link bài Final UI/UX: https://www.figma.com/proto/TLGu1J62IEn7BVmONTGKqr/FINAL-PROJECT-UI%2FUX-NGO-GIA-PHAT?page-id=0%3A1&node-id=43%3A48&starting-point-node-id=19%3A817&scaling=scale-down
